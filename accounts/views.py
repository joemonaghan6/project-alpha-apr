from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login


def signup(request):
    if request.method == "POST":  # POST is when someone pushes a button
        form = UserCreationForm(request.POST)  # This is like a dictionary of
        # all things that are posted to the form
        if form.is_valid():
            username = request.POST.get("username")
            password = request.POST.get("password1")
            user = User.objects.create_user(
                username=username, password=password
            )
            # Login
            login(request, user)
            # Redirect the user
            return redirect("home")
    else:
        form = UserCreationForm()

    return render(request, "registration/signup.html", {"form": form})
