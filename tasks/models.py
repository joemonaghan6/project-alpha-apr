from django.db import models
from django.conf import settings

USER_MODEL = settings.AUTH_USER_MODEL


class Task(models.Model):
    name = models.CharField(max_length=200)
    start_date = models.DateTimeField(auto_now_add=False)
    due_date = models.DateTimeField(auto_now_add=False)
    is_completed = models.BooleanField(default=False)
    assignee = models.ForeignKey(
        USER_MODEL,  # This connects us to the other model
        related_name="tasks",
        on_delete=models.SET_NULL,
        null=True,
    )
    project = models.ForeignKey(
        "projects.Project",
        related_name="tasks",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return str(self.name)
